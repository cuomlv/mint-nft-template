import { MINT_NFT_STEP } from '~/constants'
import { i18n } from '~/utils/helpers/i18n'

export default [
  {
    title: i18n.t('step.dataInput'),
    key: MINT_NFT_STEP.DATA_INPUT,
  },
  {
    title: i18n.t('step.dataVerification'),
    key: MINT_NFT_STEP.DATA_VERIFICATION,
  },
  {
    title: i18n.t('step.completed'),
    key: MINT_NFT_STEP.COMPLETED,
  },
]
