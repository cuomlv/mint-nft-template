import { SCREEN_PATH } from '~/constants'

export default [
  {
    name: 'mintNFT',
    router: SCREEN_PATH.MINT.MINT_NFT,
  },
  {
    name: 'mintHistory',
    router: SCREEN_PATH.MINT.MINT_HISTORY,
  },
]
