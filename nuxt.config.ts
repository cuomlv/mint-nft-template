// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: false,

  devtools: {
    enabled: true,
  },

  app: {
    head: {
      title: 'Mint Korea Horse NFT',
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
      htmlAttrs: { lang: 'en' },
      meta: [{ name: 'description', content: 'Mint Korea Horse NFT' }],
    },
  },

  css: ['~/assets/css/main.scss'],

  modules: [
    // Pinia: https://pinia.vuejs.org/ssr/nuxt.html
    [
      '@pinia/nuxt',
      {
        autoImports: ['defineStore', ['defineStore', 'definePiniaStore'], 'acceptHMRUpdate'],
      },
    ],

    // Tailwind & Nuxt 2/3: https://tailwindcss.com/docs/guides/nuxtjs
    '@nuxtjs/tailwindcss',

    // Ant Design: https://antdv.com/components/overview
    '@ant-design-vue/nuxt',
  ],

  imports: {
    dirs: ['stores'],
  },

  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },

  build: { transpile: ['yup', 'lodash'] },

  devServer: {
    port: 4000,
  },
})
