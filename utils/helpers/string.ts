export default class StringClass {
  static formatDisplay(value, firstValue, secondValue): string {
    value = value?.toString() || ''

    return value.length > 8
      ? `${value.substring(0, firstValue)}...${value.substring(value.length - secondValue)}`
      : value
  }
}
