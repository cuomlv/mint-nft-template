import BigNumber from 'bignumber.js'

export default class BigNumberClass {
  static formatNumber(bigNumber: string | number | BigNumber): string {
    return new BigNumber(bigNumber).toFormat()
  }
}
