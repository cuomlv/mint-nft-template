import dayjs, { extend } from 'dayjs'
import tz from 'dayjs/plugin/timezone'
import utc from 'dayjs/plugin/utc'

import { DATE_FORMAT, TIME_FORMAT } from '~/constants'

extend(utc)
extend(tz)

class DayJSClass {
  public static formatDate(utcTime): string {
    if (utcTime === undefined || !utcTime) {
      return ''
    }

    return dayjs(utcTime).format(DATE_FORMAT)
  }

  public static formatTime(utcTime): string {
    if (utcTime === undefined || !utcTime) {
      return ''
    }

    return dayjs(utcTime).format(TIME_FORMAT)
  }
}

export default DayJSClass
