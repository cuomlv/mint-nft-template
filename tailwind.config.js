/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './components/**/*.{vue,js,ts,jsx,tsx}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
    './app.vue',
  ],
  theme: {
    extend: {
      colors: {
        main: {
          100: '#d0f2ea',
          400: '#46c8aa',
          600: '#30ab8e',
          900: '#1b323b',
        },
        primary: {
          600: '#da4d11',
        },
        secondary: {
          100: '#fbebb1',
          400: '#ffeaab',
          600: '#fddd78',
        },
        gray: {
          500: '#555',
        },
      },
    },
  },
}
