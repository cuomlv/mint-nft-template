export const IMAGE_SRC = {
  LOGO: '/logo.png',
  DEFAULT_AVATAR: '/default-avatar.png',
  DEFAULT_NFT: '/default-nft.png',
}
