export type CardWidth = 'full' | 'half'
export type CardStyle = 'color' | 'normal'
