import { useI18n } from 'vue-i18n'

export default function useTitlePage(key?: string) {
  const { t } = useI18n()

  return key ? `${t('title')} - ${t(key)}` : t('title')
}
