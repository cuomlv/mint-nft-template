import { i18n } from '~/utils/helpers/i18n'

export default function useMintHistory() {
  const columns = ref([
    {
      title: '#',
      name: 'index',
      dataIndex: 'index',
      key: 'index',
    },
    {
      title: i18n.t('nft.label.contractAddress'),
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: i18n.t('nft.label.nftName'),
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: i18n.t('nft.label.initialOwner'),
      dataIndex: 'owner',
      key: 'owner',
    },
    {
      title: i18n.t('nft.label.status'),
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: i18n.t('nft.label.nftType'),
      dataIndex: 'type',
      key: 'type',
    },
    {
      title: i18n.t('nft.label.quantity'),
      dataIndex: 'quantity',
      key: 'quantity',
    },
    {
      title: i18n.t('nft.label.created'),
      dataIndex: 'created_at',
      key: 'created_at',
    },
    {
      title: i18n.t('nft.label.updated'),
      dataIndex: 'updated_at',
      key: 'updated_at',
    },
  ])

  const data = [
    {
      id: 1,
      address: '0xC803c27C0C0F746408154c261cE4F1da90184549',
      name: 'Mint name',
      owner: '0xC803c27C0C0F746408154c261cE4F1da90184549',
      status: 'SUCCESS',
      type: 'Type 1',
      quantity: 10,
      created_at: '2024-03-23T01:02:21.218Z',
      updated_at: '2024-03-23T01:02:21.218Z',
    },
    {
      id: 2,
      address: '0xC803c27C0C0F746408154c261cE4F1da90184549',
      name: 'Mint name',
      owner: '0xC803c27C0C0F746408154c261cE4F1da90184549',
      status: 'ERROR',
      type: 'Type 1',
      quantity: 10,
      created_at: '2024-03-23T01:02:21.218Z',
      updated_at: '2024-03-23T01:02:21.218Z',
    },
  ]

  const currentPage = 1
  const perPage = 10

  return {
    columns,
    data,
    currentPage,
    perPage,
  }
}
