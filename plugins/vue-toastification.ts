import Toast, { PluginOptions, POSITION } from 'vue-toastification'
// Import the CSS or use your own!
import 'vue-toastification/dist/index.css'
import '~/assets/css/_toastification.scss'

const filterBeforeCreate = (toast, toasts) => {
  if (toasts.filter((t) => t.content === toast.content && t.type === toast.type).length !== 0) {
    return false
  }

  return toast
}

const options: PluginOptions = {
  position: POSITION.TOP_CENTER,
  timeout: 6000,
  showCloseButtonOnHover: true,
  filterBeforeCreate,
}

export default defineNuxtPlugin(({ vueApp }) => {
  vueApp.use(Toast, options)
})
