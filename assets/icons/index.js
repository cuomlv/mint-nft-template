import AdilLogo from './adil-logo.svg'
import Logout from './logout.svg'
import ArrowBack from './arrow-back.svg'
import ArrowForward from './arrow-forward.svg'
import CVS from './csv.png'
import Success from './success.svg'
import Error from './error.svg'

export default {
  AdilLogo,
  Logout,
  ArrowBack,
  ArrowForward,
  CVS,
  Success,
  Error,
}
